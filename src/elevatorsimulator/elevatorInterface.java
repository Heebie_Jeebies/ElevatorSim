/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package elevatorsimulator;

/**
 *
 * @author Heebie__Jeebies
 */
public interface elevatorInterface {
    /**
     * returns the current floor of the elevator
     * @return flr
     */
    int getCurFloor();

    /**
     *Makes the instance goTo the specified floor.
     * Does not check if flr is in current direction of travel
     *
     * @param flr
     * flr is the floor of the building that the elevator will go to.
     */
    void goTo(int flr);

    /**
     *sleeps the thread for msDoorTime millis
     *
     */
    void openDoors();

    /**
     *adds flr to route if in the current direction of travel
     * @param flr
     * flr is the floor to be added to route
     */
    void reqFloor(int flr);

    /**
     *Runs the thread going through route and timing out if inactive
     *
     */
    void run();
    /**
     * stop the thread from running
     */
    void stop();
    /**
     * is the elevator idle
     * @return idle 
     */
    boolean isIdle();
    
    
}
