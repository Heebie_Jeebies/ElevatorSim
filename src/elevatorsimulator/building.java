package elevatorsimulator;

import java.util.LinkedList;

/**
 *
 * @author Heebie__Jeebies
 */
public class building {
    /**
     * time since the building was created
     */
    public static long timeElapsed;
    /**
     * time at which the building was created;
     */
    public static long createTime;
    /**
     * singleton instance of the building
     * no more than one building can exist in a simulation
     */
    private static building instance = null;
    /**
     * list of floors in the building
     */
    private LinkedList<person>[] floors;
    /**
     *
     */
    private simpleElevatorImpl[] elevs;
    /**
     * number of floors in building
     */
    private int flrCnt;
    /**
     * the number of elevators in the building
     */
    private int elevCnt;
    /**
     * the elevator controller 
     */
    private elevController controller;

    /**
     * create a building with floors and elevators
     *
     * @param flrCount
     */
    building(int flrs, int els,int drSecs,int fSecs, int eCap) throws InterruptedException {
        //create floors and elevators
        createTime = System.currentTimeMillis();
        floors = new LinkedList[flrs];
        elevs = new simpleElevatorImpl[els];
        flrCnt = flrs;
        elevCnt = els;
        for(LinkedList l: floors){
            l = new LinkedList<person>();
        }
        System.out.print(floors.length);
        //instantiate elevators
        //int i =1;
        for(int i=0;i<elevs.length;i++){
            elevs[i]= new simpleElevatorImpl(flrs,i,eCap,drSecs,fSecs);
        }
        controller = elevController.getInstance(elevs, flrCnt);
        Thread cntrl = new Thread(controller,"Elevator Controller");
        cntrl.start();
    }
    /**
     * get the number of floors in building
     * @return flrCnt
     */
    public int getFlrCnt() {
        return flrCnt;
    }
    /**
     * get number of elevators in building
     * @return elevCnt
     */
    public int getElevCnt(){
        return elevCnt;
    }
    /**
     * Get the array linked lists for floors
     * @return floors
     */
    public LinkedList[] getFlrs() {
        return floors;
    }
    /**
     * adds a person to their starting floor and requests an elevator pick them up
     * @param p
     * @param f 
     */
    public void addPerson(person p, int f){
        floors[f].add(p);
        System.out.println("added");
        //controller.req(p);
    }
    /**
     * gets the list of elevators
     * @return elevs
     */
    public elevatorInterface[] getElevs(){
        return elevs;
    }
    public static long getElapsedTime(){
        timeElapsed = System.currentTimeMillis()-createTime/1000;
        return timeElapsed;
    }

}
