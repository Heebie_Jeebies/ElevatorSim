
package elevatorsimulator;

import java.util.Random;

/**
 *
 * @author Heebie__Jeebies
 * @since 0.2 
 */
    public class person {
    /**
     * the floor the person started at
     */
    public int origFlr;
    /**
     * the floor on which the person is currently located
     * 
     */
    public int curFlr;
    /**
     * the destination floor of the person
     */
    public int dest;
    
    /**
     * number of floors in building
     */
    public int flrs;
    
    
    
    
    public person(int flors, int num){

        flrs = flors;
        dest = randFlr(); 
        curFlr = randFlr();
        while(dest==curFlr){
            dest = randFlr();
        }
        
        System.out.println(building.getElapsedTime() +"Person "+num+" created on floor "+curFlr+" wanting to go to floor "+dest);
        //System.out.println("Person Created on Floor: "+curFlr+" Requesting elevator to Floor: "+ dest);
    }
    
/**
 * creates random destination floor
 * @return 
 */
    private int randFlr() {
        Random rand = new Random();
        int max = flrs;
        int min = 1;
        System.out.println(max+" "+ min);
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return  randomNum;
    }
    
    
}
