/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elevatorsimulator;

import java.util.LinkedList;

public class elevController implements Runnable {

    /**
     * instance of self
     */
    private static elevController instance = null;

    LinkedList<Integer> requests = new LinkedList<>();

    private int flrCount;

    private Thread[] threads;
    private simpleElevatorImpl[] elevs;
    boolean running;
    private elevDecisionDelegate dec;

    private elevController(simpleElevatorImpl[] els, int flrCnt) throws InterruptedException {
        flrCount = flrCnt;
        elevs =  new simpleElevatorImpl[els.length];
        System.arraycopy(els, 0, elevs, 0, els.length);
        threads = new Thread[els.length];
        //create threads for elevators
        for (int i = 0; i < els.length; i++) {
            threads[i] = new Thread(els[i],"Elevator: "+i);
            //threads[i].wait();
        }
        //start elevators
        System.out.println("Starting Elevator Threads");
        
        
        
    }

    public static elevController getInstance(simpleElevatorImpl[] elevs, int flrCnt) throws InterruptedException {
        if (instance == null) {
            System.out.println("Elevator Controller Created");
            //synchronized (instance) {
            instance = new elevController(elevs, flrCnt);
            //}
        }
        return instance;
    }

    @Override
    public void run() {
        System.out.println("Controller Running");
        running = true;
        startElevators();
        while (running) {
            //System.out.println(requests.size());
            if(!requests.isEmpty()) {
               int f = requests.pop();
                elevDecisionDelegate.chooseElev(elevs, f).reqFloor(f);

                
            }
        }
    }
    public void stop(){
        running = false;
    }
    public void reqFloor(int flr) {
        if (flr > 0 && flr <= flrCount) {
            requests.add(flr);
        }
    }

    private void startElevators() {
        for (Thread t : threads) {

            t.start();
        }
        //threads[0].start();

    }

    void req(person p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
