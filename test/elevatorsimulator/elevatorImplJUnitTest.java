/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elevatorsimulator;
import java.security.InvalidParameterException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Colin Cruse
 * 
 */
public class elevatorImplJUnitTest {
    
    public elevatorImplJUnitTest() {
    }
    @Test public void testIsIdle(){
        simpleElevatorImpl elev = new simpleElevatorImpl(16,1);
        Thread Elev = new Thread(elev);
        Elev.start();
        assert(elev.isIdle());
        elev.reqFloor(15);
        try{
        elev.wait(200);}
        catch(Exception e){
            System.out.print(e);
        }
        assert(!elev.isIdle());
    }
    @Test public void testreqFloor(){
        simpleElevatorImpl elev = new simpleElevatorImpl(16,1);
        Thread Elev = new Thread(elev);
        Elev.start();
        elev.reqFloor(12);
        elev.reqFloor(5);
        elev.reqFloor(16);
        
    }
    @Test public void testopenDoors(){
        simpleElevatorImpl elev = new simpleElevatorImpl(16,1);
        Thread Elev = new Thread(elev);
        Elev.start();
        elev.openDoors();
    }
    @Test public void testflrInCurDir(){
        simpleElevatorImpl elev = new simpleElevatorImpl(16,1);
        Thread Elev = new Thread(elev);
        Elev.start();
        simpleElevatorImpl elev2 = new simpleElevatorImpl(16,2);
        Thread Elev2 = new Thread(elev2);
        Elev2.start();
        elev2.reqFloor(15);
        elev2.reqFloor(16);
        elev.reqFloor(15);
        elev.reqFloor(1);
        elev.reqFloor(16);
        assert(elev.getRoute().size() == elev2.getRoute().size());
        
    }

    
    
}
