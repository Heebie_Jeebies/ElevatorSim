/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elevatorsimulator;

/**
 *
 * @author Heebie__Jeebies
 */
class elevDecisionDelegate {

    /**
     * decides which elevator will go to a specified floor
     *
     * @param elevs
     * @param reqFlr
     * @return
     */

    public static elevatorInterface chooseElev(elevatorInterface[] elevs, int reqFlr) {
        synchronized (elevs) {
            elevs.notifyAll();
        }
        for (elevatorInterface e : elevs) {
            if (e.isIdle()) {
                return (e);
            }
        }
        return(elevs[0]);

    }
}
