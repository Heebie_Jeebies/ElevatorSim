package elevatorsimulator;

import java.security.InvalidParameterException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Heebie__Jeebies
 */
public class simpleElevatorImpl implements elevatorInterface, Runnable {

    /**
     * int value representing the currentFlr the elevator is located at)
     */
    private int currentFlr;
    /**
     * The number of floors that the elevator must reach
     */
    private int floors;
    /**
     * Route is the list of destinations the elevator is en route to
     */
    private LinkedList<Integer> route = new LinkedList<>();
    /**
     * idle is true when the elevator is not moving or en route anywhere
     */
    private boolean idle = true;
    /**
     * Default floor, where the elevator returns after timing out
     */
    private int defFloor;
    /**
     * Time for the doors to open
     *
     * @see #openDoors()
     */
    private int msDoorTime;
    /**
     * the number of milliseconds for the elevator to move 1 floor;
     *
     * @see #goTo()
     */
    private int msFloorTime;
    /**
     * The time at which the elevator was created
     */
    private long startTime;
    /**
     * true if the elevator is running false if not
     */
    private boolean running = true;
    /**
     * the number of milliseconds before an elevator returns to its default
     * floor
     */
    private int msBeforeTimeout = 15000;
    /**
     * The designated elevator number
     */
    private int elevNum;
    /**
     * Maximum number of persons the elevator can hold
     */
    private int maxCap;

    /**
     * Elevator Constructor takes the number of floors in the building, the
     * elevator number, max capacity of people, door and floor time as params
     *
     * @param flrs
     * @param elevatorNum
     * @param maxCap
     * @param doorTime
     * @param floorTime
     */
    public simpleElevatorImpl(int flrs, int elevatorNum, int maxCap, int doorTime, int floorTime) {
        System.out.print(building.getElapsedTime()+" Constructing Elevator:" + elevatorNum + "\n");
        try {
            setFloors(flrs);
            setCurFloor(1);
            setDefFloor(1);
            setDoorTime(doorTime);
            setFloorTime(floorTime);
            setCap(maxCap);
            startTime = System.currentTimeMillis();
            elevNum = elevatorNum;

        } catch (Exception e) {
            System.out.print(e);
        }

    }

    /**
     * requests the elevator to go to the specified floor only goes to floor if
     * in current direction of movement or if elevator is idle
     *
     * @param flr
     */
    @Override
    public void reqFloor(int flr) {
        if (flrInCurDir(flr)) {
            route.add(flr);
            System.out.println("added " + flr);
        } else {
            System.out.println(flr + " Not in direction of travel");
            //throw new InvalidParameterException("Floor Not In Current Direction Of Travel");
        }
    }

    /**
     * open the doors for people to enter the elevator takes
     * msDoorTimemsDoorTime ms
     */
    @Override
    public void openDoors() {
        try {
            System.out.print((System.currentTimeMillis() - startTime) / 1000 + "secs Elevator " + elevNum + " Opening Doors on floor: " + Integer.toString(currentFlr) + "\n");
            Thread.sleep(msDoorTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(simpleElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * moves the elevator to the specified floor takes msFloorTime per flr in
     * between currentFlr and flr
     *
     * @param flr
     */
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void goTo(int flr) {
        if (flr >= 1) {
            if (flr == currentFlr) {
                openDoors();
                //do nothing, go straight to opening doors
            } else if (flr > currentFlr) {
                System.out.print(building.getElapsedTime()+ "  secs Elevator " + elevNum + " Going to floor: " + flr + "\n");
                for (int f = currentFlr; f <= flr; f++) {
                    currentFlr = f;
                    try {
                        Thread.sleep(msFloorTime);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(simpleElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (flr < currentFlr) {
                System.out.print(building.getElapsedTime()+ " secs Elevator " + elevNum + " Going to floor: " + flr + "\n");
                for (int f = currentFlr; f >= flr; f--) {
                    currentFlr = f;
                    try {
                        Thread.sleep(msFloorTime);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(simpleElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            //openDoors(); //arrived
        } else {
            throw new InvalidParameterException("flrs must be >=1: " + flr);
        }
    }

    /**
     * starts the elevator and runs the thread
     */
    @Override
    public void run() {
        System.out.println("Elevator: "+elevNum+ "Running");
        running = true;
        while (running) {
            if (!route.isEmpty()) {//route not empty
                idle = false;
                if (route.peek() != null) {
                    goTo(route.pop());

                    openDoors();
                }
            } else { //route is empty
                if (getCurFloor() != defFloor) {//not at default floor
                    idle = true;
                    synchronized (this) {
                        try {
                            System.out.println(building.getElapsedTime()+" Elevator " + elevNum + " is idleing at floor: " + getCurFloor());
                            this.wait(msBeforeTimeout);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(simpleElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        if (route.isEmpty()) {
                            gotoDef();//elevator was not requested while timing out
                        } else {
                            break;
                        }
                    }
                } else {
                    idle = true;
                    //System.out.println("Idleing at: " + this.defFloor);
                }

            }
        }
    }

    private void gotoDef() {
        goTo(defFloor);
//        try {
//            synchronized()
//            this.wait();
//        } catch (InterruptedException ex) {
//            Logger.getLogger(simpleElevatorImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
        System.out.println(building.getElapsedTime()+" Waiting at default floor");
    }
    

    /**
     * stop the elevator running
     */
    public void stop() {
        running = false;
    }

    /**
     * returns the current floor of the elevator
     *
     * @return currentFlr
     */
    @Override
    public int getCurFloor() {
        return currentFlr;
    }

    /**
     * is the elevator idle
     *
     * @return idle
     */
    @Override
    public boolean isIdle() {
        return idle;
    }

    /**
     * returns the linked list route of the elevator
     *
     * @return route
     */
    public LinkedList getRoute() {
        return route;
    }

    /**
     * sets the number of floors the elevator services
     *
     * @param flrs
     */
    private void setFloors(int flrs) {
        if (flrs >= 1) {
            floors = flrs;
        } else {
            throw new InvalidParameterException("Floors have to be positive");
        }
    }

    /**
     * sets the current floor of the elevator
     *
     * @param i
     */
    private void setCurFloor(int i) {
        if (i >= defFloor && i <= defFloor + floors) {
            currentFlr = i;

        } else {
            throw new InvalidParameterException("Floor has to be within the range of the building");
        }
    }

    /**
     * sets the default floor of the elevator where it returns after timing out
     * and where it starts from
     *
     * @param i
     */
    private void setDefFloor(int i) {
        // only has to be an integer as floors can be negative 
        if (i >= 1 && i <= floors) {
            defFloor = i;
        } else {
            throw new InvalidParameterException("Default floor must be greater than one and at max be the top floor");
        }
    }

    /**
     * returns if the specified flr is in the current direction of movement for
     * the elevator
     *
     * @param flr
     * @return boolean in current direction of movement
     */
    private boolean flrInCurDir(int flr) {
        if (route.isEmpty()) {
            return true;
        } else { //route not empty
            return (route.peek() > getCurFloor() && flr > getCurFloor() || (route.peek() < getCurFloor() && flr < getCurFloor()));
        }
    }

    /**
     * sets the number of milliseconds it takes to open the elevator doors
     *
     * @param ms
     */
    private void setDoorTime(int ms) {
        if (ms >= 0) {
            msDoorTime = ms;
        } else {
            throw new InvalidParameterException("Time cannot be negative");
        }
    }

    /**
     * sets the number of milliseconds it takes to go 1 floor
     *
     * @param ms
     */
    private void setFloorTime(int ms) {
        if (ms >= 0) {
            msFloorTime = ms;
        } else {
            throw new InvalidParameterException("Time cannot be negative");
        }
    }

    /**
     * sets the maximum capacity of the elevator
     *
     * @param maxCap
     */
    private void setCap(int cap) {
        if (cap > 0) {
            maxCap = cap;
        } else {
            throw new InvalidParameterException("Elevator must be able to hold at least one ");
        }
    }
}
